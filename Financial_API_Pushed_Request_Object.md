# Financial-grade API: Pushed Request Object

The Financial-grade API working group's pushed request object specification is deprecated and no longer maintained.

The FAPI working group suggest that implementers use OAuth 2.0 Pushed Authorization Requests ('PAR'), which are being standardised at the IETF OAuth working group.

The latest version of the PAR specification may be found here:

https://tools.ietf.org/html/draft-ietf-oauth-par

For historical reference, the FAPI WG's final version of this specification can be viewed here:

https://bitbucket.org/openid/fapi/src/0f7d3ab8c78faa0a35a5cf1fac1a05c6501d6cf2/Financial_API_Pushed_Request_Object.md
